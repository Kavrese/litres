package com.example.litres.Fragments.CatalogFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.litres.R
import kotlinx.android.synthetic.main.fragment_catalog_with_ads.*


class CatalogContentFragment(
    private val variantContent: VariantContent,
    private val onAttachContentRecyclerView: OnAttachRecyclerView? = null,
    private val onAttachAdsPager: OnAttachViewPagerAds? = null
    ): Fragment() {

    companion object {
        interface OnAttachViewPagerAds{
            fun onAttach(pagerAds: ViewPager2)
        }
    }

    interface OnAttachRecyclerView{
        fun onAttach(recyclerView: RecyclerView)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(variantContent.id_layout, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (variantContent == VariantContent.WITH_ADS){
            onAttachAdsPager?.onAttach(pager_ads)
        }else if (variantContent in arrayListOf(VariantContent.WITHOUT_ADS,
                VariantContent.SIMPLE_RECYCLER
            )) {
            onAttachContentRecyclerView?.onAttach(catalog_fragment_rec)
        }
    }
}

enum class VariantContent(val id_layout: Int){
    WITH_ADS(R.layout.fragment_catalog_with_ads),
    WITHOUT_ADS(R.layout.fragment_catalog_without_ads),
    SIMPLE_RECYCLER(R.layout.fragment_catalog_rec),
}