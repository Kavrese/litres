package com.example.litres.View

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.example.litres.Models.ModelBook
import com.example.litres.R

class BookView: LinearLayout {
    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet): super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int): super(context, attrs, defStyle)

    private var modelBook: ModelBook? = null
    var typeBook: TypeBook = TypeBook.LARGE_WIDTH_VERTICAL_ITEM

    fun setBook(modelBook: ModelBook){
        this.modelBook = modelBook
        setTypeFromBook()
        fillContent()
    }

    private fun fillContent(){
        fun fillAllType(){

        }
        fun fillHorizontalType(){

        }
        fun fillVerticalType(){

        }

        fillAllType()
        when(typeBook){
            TypeBook.AUDIO_BOOK_HORIZONTAL_ITEM, TypeBook.PAGE_BOOK_HORIZONTAL_ITEM -> fillHorizontalType()
            TypeBook.LARGE_WIDTH_VERTICAL_ITEM -> fillVerticalType()
        }
    }

    private fun setTypeFromBook(){
        typeBook = TypeBook.parseStrType(modelBook!!.type)
        replaceViewTypeBook()
    }

    private fun replaceViewTypeBook(){
        removeAllViews()
        inflate(context, typeBook.id_layout, this)
    }
}

enum class TypeBook(val id_layout: Int){
    PAGE_BOOK_HORIZONTAL_ITEM(R.layout.item_page_book),
    AUDIO_BOOK_HORIZONTAL_ITEM(R.layout.item_audio_book),
    LARGE_WIDTH_VERTICAL_ITEM(R.layout.item_book);

    companion object {
        fun parseStrType(str_type: String): TypeBook{
            return when(str_type){
                "text" -> PAGE_BOOK_HORIZONTAL_ITEM
                "audio" -> AUDIO_BOOK_HORIZONTAL_ITEM
                else -> PAGE_BOOK_HORIZONTAL_ITEM
            }
        }
    }
}