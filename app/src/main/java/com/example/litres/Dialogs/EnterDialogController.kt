package com.example.litres.Dialogs

import android.app.Activity
import com.example.litres.Class.interfaces.OnCallbackEnterDialog

class EnterDialogController(activity: Activity, onCallbackEnterDialog: OnCallbackEnterDialog?=null){

    var identificationDialog: IdentificationDialog =
        IdentificationDialog(activity.applicationContext, onCallbackEnterDialog)
    var identificationBookTicketDialog: IdentificationBookTicketDialog =
        IdentificationBookTicketDialog(activity.applicationContext, onCallbackEnterDialog)

    init {
        identificationDialog.show()
    }

    fun dismiss(){
        identificationBookTicketDialog.dismiss()
        identificationBookTicketDialog.dismiss()
    }
}