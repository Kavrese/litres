package com.example.litres.Dialogs

import android.content.Context
import android.os.Bundle
import com.example.litres.Class.interfaces.OnCallbackAuthorizationDialog
import com.example.litres.Class.interfaces.OnCallbackEnterDialog
import com.example.litres.R

class IdentificationBookTicketDialog(context: Context, var onCallbackEnterTicketDialog: OnCallbackAuthorizationDialog?=null): PlatformDialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_enter_ticket_dialog)
    }
}
