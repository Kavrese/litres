package com.example.litres.Class.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

interface OnClickElement<T>{
    fun onClick(element: T, position: Int)
}

abstract class DynamicRecyclerView<T>(protected val data: MutableList<T> = mutableListOf()): RecyclerView.Adapter<SimpleViewHolder>(){

    fun add(element: T, index: Int=data.size){
        data.add(index, element)
        notifyDataSetChanged()
    }

    fun addAll(new_data: List<T>){
        data.clear()
        data.addAll(new_data)
        notifyDataSetChanged()
    }

    fun clear(){
        data.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.size
}

class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
