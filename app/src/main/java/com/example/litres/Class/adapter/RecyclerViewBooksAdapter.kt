package com.example.litres.Class.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.litres.Models.ModelBook
import com.example.litres.R

class RecyclerViewBooksAdapter(private val books: MutableList<ModelBook> = mutableListOf(),
                               val clickElement: OnClickElement<ModelBook>? = null):
    DynamicRecyclerView<ModelBook>(data=books) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_ad, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {

    }
}