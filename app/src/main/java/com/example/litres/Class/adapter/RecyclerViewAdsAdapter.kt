package com.example.litres.Class.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.litres.Models.ModelAd
import com.example.litres.R

class RecyclerViewAdsAdapter(
    data: MutableList<ModelAd> = mutableListOf(),
    var onClickElement: OnClickElement<ModelAd>?=null
): DynamicRecyclerView<ModelAd>(data=data) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_ad, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.itemView.setOnClickListener { onClickElement?.onClick(data[position], position) }
    }
}