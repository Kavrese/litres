package com.example.litres.Class.interfaces

interface OnGetData<T> {
    fun onGet(data: T)
    fun onFail(message: String)
}