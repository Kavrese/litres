package com.example.litres.Class.NetworkConnection

import com.example.litres.Class.interfaces.OnGetData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ConnectControllerAcceptPoint{
    val connectController = ConnectController()
}

class ConnectController {

    val retrofit = initRetrofit(URLS.MAIN.url)

    companion object {
        fun initRetrofit(baseUrl: String): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    fun <T> Call<T>.request(onGetData: OnGetData<T>){
        this.enqueue(object: Callback<T>{
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful && response.body() != null){
                    onGetData.onGet(response.body()!!)
                }else{
                    onGetData.onFail("Response body null")
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                onGetData.onFail(t.message ?: t.localizedMessage ?: "Unknown fail error")
            }
        })
    }

    enum class URLS(val url: String){
        MAIN("");
        override fun toString(): String {
            return url
        }
    }
}