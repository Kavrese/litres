package com.example.litres.Class.interfaces

import com.example.litres.Models.ModelMainInfo

interface OnCallbackAuthorizationDialog{
    fun onEnter(login: String, password: String)
}

interface OnCallbackEnterDialog: OnCallbackAuthorizationDialog{
    fun onReg(login: String, password: String)
}