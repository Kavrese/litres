package com.example.litres.Class.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.litres.Models.ModelBook
import com.example.litres.Models.ModelCategory
import com.example.litres.R

class RecyclerViewCategoryBooksAdapter(private val books: MutableList<ModelCategory> = mutableListOf(),
                                       val clickElement: OnClickElement<ModelCategory>? = null):
    DynamicRecyclerView<ModelCategory>(data=books) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_category_books, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {

    }
}