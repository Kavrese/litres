package com.example.litres.Class

import android.app.Activity
import android.content.Context
import com.example.litres.Models.ModelMainInfo
import com.example.litres.Models.ModelUser

object UserSessionAcceptPoint{
    val userSession: UserSession? = null
}

class UserSession{
    private var modelUser: ModelUser? = null
    private var commonMemoryController: CommonMemoryController
    private var userMemoryController: UserMemoryController? = null

    constructor(activity: Activity): this(activity.applicationContext)
    private constructor(context: Context){
        commonMemoryController = CommonMemoryController(context)
    }

    fun enterUser(context: Context, modelUser: ModelUser, password: String){
        userMemoryController = UserMemoryController(context, modelUser)
        userMemoryController!!.saveModelUserMainInfo(password)
        commonMemoryController.saveLastUser(modelUser.nickname, password)
    }

    fun exitUser(){
        modelUser = null
        userMemoryController = null
    }

    fun getLastDataAuth(): ModelMainInfo{
        return commonMemoryController.getLastModelInfoUser()
    }

    class UserMemoryController(context: Context, private val modelUser: ModelUser): MemoryController(context, modelUser.nickname) {
        fun saveModelUserMainInfo(password: String){
            sh.edit()
                .putString("id", modelUser.id)
                .putString("email", modelUser.email)
                .putString("password", password)
                .apply()
        }

        fun getModelUserMainInfo(): ModelMainInfo{
            return ModelMainInfo(sh.getString("email", "")!!, sh.getString("password", "")!!)
        }
    }

    class CommonMemoryController(context: Context): MemoryController(context, "default") {
        fun saveLastUser(email: String, password: String){
            sh.edit()
                .putString("last_email", email)
                .putString("last_password", password)
                .apply()
        }

        fun getLastModelInfoUser(): ModelMainInfo {
            return ModelMainInfo(sh.getString("last_email", "")!!, sh.getString("last_password", "")!!)
        }
    }

    open class MemoryController(context: Context, key: String) {
        protected val sh = context.getSharedPreferences(key, Context.MODE_PRIVATE)!!
    }
}