package com.example.litres.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.children
import androidx.core.view.iterator
import com.example.litres.Class.adapter.SimpleFragmentStateAdapter
import com.example.litres.Fragments.MainFragments.*
import com.example.litres.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pager_main.adapter = SimpleFragmentStateAdapter(this, arrayListOf(
            CatalogFragment(), SearchFragment(), ReadFragment(), MyBookFragment(), ProfileFragment()
        ))

        pager_main.isUserInputEnabled = false

        bottom_nav_view.setOnItemSelectedListener {
            return@setOnItemSelectedListener if (it.title != resources.getString(R.string.item_reader)){
                pager_main.setCurrentItem(bottom_nav_view.menu.children.indexOf(it), true)
                true
            }else{
                startActivity(Intent(this, ReadActivity::class.java))
                false
            }
        }
    }
}