package com.example.litres.Models

data class ModelMainInfo(
    val email: String,
    val password: String
)