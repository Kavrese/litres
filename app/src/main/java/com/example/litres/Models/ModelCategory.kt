package com.example.litres.Models

data class ModelCategory(
    val title: String,
    val books: List<ModelBook>,
)
