package com.example.litres.Models

data class ModelBook(
    val id: Int,
    val title: String,
    val img: String,
    val rating: Float,
    val type: String,
    val author: String,
    val cost: Float,
    val count_review: Int
)
