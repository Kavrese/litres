package com.example.litres.Models

data class ModelUser(
    val nickname: String,
    val email: String,
    val id: String,
    val rouble_wallet: Int,
    val bonus_wallet: Int
)
